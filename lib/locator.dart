import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'dart:developer';

class Locator {
  LatLng _centerLatLng;
  LatLng _currentUserLatLn;

  LocationData _currentLocation;
  Location _locationWrapper;

  VoidCallback _pinMarker;

  bool _serviceEnabled;

  Locator(double lat, double lng, {@required VoidCallback onLocationChanged}) {
    _centerLatLng = LatLng(lat, lng);
    _locationWrapper = new Location();
    this._pinMarker = onLocationChanged;
    _checkLocationPermission();
  }

  Future<LocationData> _getCurrentLocation() async {
    return _locationWrapper.getLocation();
  }

  void _checkLocationPermission() async {
    _serviceEnabled = await _locationWrapper.serviceEnabled();
    if (!_serviceEnabled) {
      await _getCurrentLocation().then((value) {
        _currentLocation = value;
        _setLocationListener();
      });
    } else {
      _setLocationListener();
    }
  }

  void _setLocationListener() {
    _locationWrapper.onLocationChanged.listen((LocationData newLocation) {
      _currentLocation = newLocation;
      _currentUserLatLn =
          LatLng(_currentLocation.latitude, _currentLocation.longitude);
      this._pinMarker();
    });
  }

  LatLng get centerLatLng => _centerLatLng;

  LatLng get currentUserLatLn => _currentUserLatLn;
}
