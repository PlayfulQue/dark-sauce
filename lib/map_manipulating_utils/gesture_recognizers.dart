import 'package:flutter/gestures.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MyEagerGestureRecognizer extends EagerGestureRecognizer {

  Function _callback;

  MyEagerGestureRecognizer(this._callback);

  @override
  void resolve(GestureDisposition disposition) {
    super.resolve(disposition);

    _callback();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }



}
