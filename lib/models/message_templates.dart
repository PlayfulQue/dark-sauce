enum MessageTemplate {
  simple,
  exclamation,
  question,
  simpleDouble,
  exclamationDouble,
  questionDouble
}

extension MessageTemplatesExtension on MessageTemplate {
  String get stringTemplate {
    switch (this) {
      case MessageTemplate.simple:
        return "*********";
      case MessageTemplate.exclamation:
        return "*********!";
      case MessageTemplate.question:
        return "*********?";
      case MessageTemplate.simpleDouble:
        return "********* *********";
      case MessageTemplate.exclamationDouble:
        return "********* *********!";
      case MessageTemplate.questionDouble:
        return "********* *********?";
    }
  }
}
