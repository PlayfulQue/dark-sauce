import 'package:darksauce/models/message_templates.dart';
import 'package:flutter/material.dart';

class PrepareToPlaceDialog extends StatelessWidget {
  final Function proceedCallback;
  final Function cancelListener;

  PrepareToPlaceDialog(
      {@required this.proceedCallback, @required this.cancelListener});

  @override
  Widget build(BuildContext context) {
    var _dialogOffset = MediaQuery.of(context).size.width * 0.05;
    return Align(
      alignment: Alignment(0, 0.80),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: _dialogOffset),
        child: DialogWrapper(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Text(
                  "Разместить сообщение здесь?",
                  style: TextStyle(
                      fontFamily: 'DarkSoulsTrajan',
                      color: Colors.white,
                      fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    createFlatButton(text: "Да", listener: proceedCallback),
                    createFlatButton(text: "Отмена", listener: cancelListener),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DialogWrapper extends StatelessWidget {
  final Widget child;

  DialogWrapper({@required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          border: Border.all(width: 1.0, color: Colors.white),
          color: Colors.black,
        ),
        child: child);
  }
}

class MessageContentSelector extends StatefulWidget {
  @override
  State createState() => MessageContentSelectorState();
}

class MessageContentSelectorState extends State {
  MessageTemplate _selectedItem = MessageTemplate.simple;

  void _changeSelectedItem(MessageTemplate messageTemplate) {
    setState(() {
      _selectedItem = messageTemplate;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: DialogWrapper(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    "Введите содержимое сообщения",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'DarkSoulsTrajan',
                      fontSize: 16.0,
                    ),
                  ),
                ),
                DropdownButton<MessageTemplate>(
                  value: _selectedItem,
                  dropdownColor: Colors.black,
                  onChanged: _changeSelectedItem,
                  items: MessageTemplate.values
                      .map((MessageTemplate messageTemplate) {
                    return DropdownMenuItem<MessageTemplate>(
                      value: messageTemplate,
                      child: Container(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Text(
                            messageTemplate.stringTemplate,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'DarkSoulsTrajan',
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      createFlatButton(text: "Разместить", listener: () {}),
                      createFlatButton(
                          text: "Отмена",
                          listener: () {
                            Navigator.of(context).pop(null);
                          }),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Future<String> initPlaceMessageDialog(BuildContext context) {
  return showDialog<String>(
      barrierDismissible: true,
      context: context,
      builder: (context) => MessageContentSelector());
}

Widget createFlatButton({String text, @required Function listener}) {
  return Expanded(
    child: FlatButton(
      child: Text(
        text,
        style: TextStyle(
            fontFamily: 'DarkSoulsTrajan', color: Colors.white, fontSize: 15.0),
      ),
      onPressed: listener,
      splashColor: Colors.white,
    ),
  );
}
