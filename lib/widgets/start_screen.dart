import 'package:darksauce/widgets/map_screen.dart';
import 'package:flutter/material.dart';
import 'dart:developer';

class StartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Builder(builder: (context) {
        return GestureDetector(
          onTap: () {
            pushMapWidget(context);
          },
          child: Scaffold(
            backgroundColor: Colors.black,
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset("assets/images/logo.png"),
                  Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: Text(
                      "Нажмите, чтобы продолжить",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "DarkSoulsTrajan",
                          fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });

  void pushMapWidget(context) {
    Route route = MaterialPageRoute(builder: (context) => MapWidget());
    Navigator.of(context).pushReplacement(route);
  }

}
