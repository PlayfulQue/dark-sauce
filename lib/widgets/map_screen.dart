import 'dart:collection';

import 'package:darksauce/map_manipulating_utils/gesture_recognizers.dart';
import 'package:darksauce/widgets/dialogs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:developer';
import 'package:darksauce/locator.dart';
import 'package:darksauce/utils/icon_utils.dart';

class MapWidget extends StatefulWidget {
  @override
  MapWidgetState createState() => MapWidgetState();
}

class MapWidgetState extends State<MapWidget> {
  Locator _locator;

  GoogleMapController mapController;
  String _mapStyle;

  final double _cameraFollowsZoom = 13;
  final double _messagePlacingZoom = 17;
  final double _cameraTilt = 80;
  MapStates _mapState = MapStates.followingUser;

  Map _markers = HashMap<String, Marker>();
  final String _userMarkerStringId = "userPin";
  Marker _currentMarker;

//  //current to-be message string id
//  String _currentMarkerStrId;

  BitmapDescriptor _userMarkerIcon;
  BitmapDescriptor _messageMarkerIcon;

  double _dialogOffset;

  @override
  void initState() {
    super.initState();

    rootBundle.loadString('assets/styles/map_style.txt').then((stringStyle) {
      _mapStyle = stringStyle;
    });

    _loadIcons();

    _locator = new Locator(61.498151, 23.761025, onLocationChanged: () {
      if (_mapState == MapStates.followingUser) {
        _animateCamera(focusPosition: this._locator.currentUserLatLn, zoom: _cameraFollowsZoom);
      }
      setState(() {
        _updateUserMarker();
      });
    });
  }

  void _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
    mapController.setMapStyle(_mapStyle);
  }

  void _loadIcons() async {
    _userMarkerIcon = await getBitmapDescriptorFromAssetBytes(
        'assets/images/praise.png', 200);
    _messageMarkerIcon =
        await getBitmapDescriptorFromAssetBytes('assets/images/sign.png', 300);
  }

  void _updateUserMarker() async {
    var userPosition = LatLng(_locator.currentUserLatLn.latitude,
        _locator.currentUserLatLn.longitude);

    if (_markers[_userMarkerStringId] == null) {
      _markers.remove(_userMarkerStringId);
    }
    _markers[_userMarkerStringId] = _createMarker(userPosition,
        markerStringId: _userMarkerStringId, markerType: MarkerTypes.user);
  }

  void _animateCamera(
      {@required LatLng focusPosition, @required double zoom}) async {
    var cameraPosition = CameraPosition(
      zoom: zoom,
      target: focusPosition,
    );
    mapController.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  Marker _createMarker(LatLng position,
      {MarkerTypes markerType = MarkerTypes.pointer, String markerStringId}) {
    BitmapDescriptor icon;
    switch (markerType) {
      case MarkerTypes.user:
        icon = _userMarkerIcon;
        break;
      case MarkerTypes.message:
        icon = _messageMarkerIcon;
        break;
      case MarkerTypes.pointer:
        icon = BitmapDescriptor.defaultMarker;
    }

    markerStringId ??= _generateMarkerId(position: position);
    MarkerId markerId = MarkerId(markerStringId);
    return Marker(
        markerId: markerId,
        position: position,
        icon: icon,
        onTap: () {
          log("The marker id is $markerStringId");
        });
  }

  void _dropPlacingMessage() {
    _mapState = MapStates.freeCamera;
    _currentMarker = null;
  }

  String _generateMarkerId({@required LatLng position}) {
    return DateTime.now().millisecondsSinceEpoch.toString() +
        (position.latitude + position.longitude).floor().toString();
  }

  void _prepareToPlaceMessage(LatLng position) {
    _animateCamera(focusPosition: position, zoom: _messagePlacingZoom);
    _currentMarker = _createMarker(position);
    _mapState = MapStates.initMessagePlacing;
  }

  @override
  Widget build(BuildContext context) {
    var markers;

    if (_mapState == MapStates.initMessagePlacing || _mapState == MapStates.placingMessage) {
      markers = HashMap<String, Marker>();
      markers[_currentMarker.markerId.value] = _currentMarker;
    } else {
      markers = _markers;
    }

    return Scaffold(
      backgroundColor: Colors.blue,
      body: Stack(
        children: <Widget>[
          GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
                target: _locator.centerLatLng, zoom: _cameraFollowsZoom),
            markers: markers.values.toSet(),
            gestureRecognizers: Set()
              ..add(
                Factory<OneSequenceGestureRecognizer>(
                  () => MyEagerGestureRecognizer(() {
                    switch (_mapState) {
                      case MapStates.followingUser:
                        _dropPlacingMessage();
                        break;
                    }
                  }),
                ),
              ),
            compassEnabled: true,
            zoomControlsEnabled: false,
            mapToolbarEnabled: false,
            onTap: (latLng) {
              setState(() {
                switch (_mapState) {
                  case MapStates.freeCamera:
                    _prepareToPlaceMessage(latLng);
                    break;
                  case MapStates.initMessagePlacing:
                    setState(() {
                      _dropPlacingMessage();
                    });
                }
              });
            },
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 0, 5.0, 20.0),
              child: FloatingActionButton(
                onPressed: () {
                  setState(() {
                    if (_mapState == MapStates.initMessagePlacing) {
                      _dropPlacingMessage();
                    }
                    _mapState = MapStates.followingUser;
                    _animateCamera(
                        focusPosition: LatLng(
                            _locator.currentUserLatLn.latitude,
                            _locator.currentUserLatLn.longitude),
                        zoom: _cameraFollowsZoom);
                  });
                },
                backgroundColor: Colors.transparent,
                child: Image.asset('assets/images/eye.png'),
              ),
            ),
          ),
          _mapState == MapStates.initMessagePlacing
              ? PrepareToPlaceDialog(
                  proceedCallback: () {
                    setState(() {
                      _mapState = MapStates.placingMessage;
                      initPlaceMessageDialog(context);
                    });
                  },
                  cancelListener: () {
                    setState(() {
                      _mapState = MapStates.freeCamera;
                      _dropPlacingMessage();
                    });
                  },
                )
              : Container()
        ],
      ),
    );
  }
}

enum MapStates {
  freeCamera,
  followingUser,
  followingUserWithRotation,
  initMessagePlacing,
  placingMessage,
  readingMessage
}

enum MarkerTypes { user, message, pointer }
